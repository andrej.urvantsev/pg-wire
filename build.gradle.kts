import com.diffplug.gradle.spotless.SpotlessExtension
import com.github.spotbugs.snom.SpotBugsExtension
import com.github.spotbugs.snom.SpotBugsTask

plugins {
    id("com.github.ben-manes.versions")
    id("net.researchgate.release")

    id("com.github.spotbugs") apply false
    id("com.diffplug.spotless") apply false
}

description = "JustSQL"

val String.v: String get() = rootProject.extra["$this.version"] as String

allprojects {
    group = "se.urvantsev.jsql"
    version = "0.0.1-SNAPSHOT"

    repositories {
        mavenCentral()
    }

    plugins.withType<JavaLibraryPlugin> {
        plugins.apply("com.github.spotbugs")
        plugins.apply("com.diffplug.spotless")
        plugins.apply("checkstyle")
        plugins.apply("jacoco")
        plugins.apply("maven-publish")

        dependencies.add("compileOnly", "com.github.spotbugs:spotbugs-annotations:${"spotbugs".v}")
        dependencies.add("testCompileOnly", "com.github.spotbugs:spotbugs-annotations:${"spotbugs".v}")

        dependencies.add("spotbugsPlugins", "jp.skypencil.findbugs.slf4j:bug-pattern:${"spotbugs-slf4j".v}")
        dependencies.add("spotbugsPlugins", "com.mebigfatguy.sb-contrib:sb-contrib:${"spotbugs-contrib".v}")
        dependencies.add("spotbugsPlugins", "com.h3xstream.findsecbugs:findsecbugs-plugin:${"findsecbugs".v}")

        dependencies.add("testImplementation", "org.junit.jupiter:junit-jupiter-api:${"junit".v}")
        dependencies.add("testImplementation", "org.junit.jupiter:junit-jupiter-params:${"junit".v}")
        dependencies.add("testRuntimeOnly", "org.junit.jupiter:junit-jupiter-engine:${"junit".v}")
        dependencies.add("testImplementation", "org.assertj:assertj-core:${"assertj".v}")

        configure<CheckstyleExtension> {
            toolVersion = "checkstyle".v
        }
        tasks.named<Checkstyle>("checkstyleMain") {
            reports {
                xml.required.set(true)
                html.required.set(true)
            }
        }
        tasks.named<Checkstyle>("checkstyleTest") {
            enabled = false
        }

        configure<SpotlessExtension> {
            java {
                removeUnusedImports()
                target(project.file("src/main/java"), project.file("src/test/java"))
                //palantirJavaFormat()
                googleJavaFormat("1.16.0")
            }
        }

        configure<SpotBugsExtension> {
            toolVersion.set("spotbugs".v)
            excludeFilter.set(file("findbugs-exclude.xml"))
        }
        tasks.named<SpotBugsTask>("spotbugsMain") {
            ignoreFailures = true
            effort.set(com.github.spotbugs.snom.Effort.MAX)
            reportLevel.set(com.github.spotbugs.snom.Confidence.LOW)
            reports.create("html") {
                enabled = true
            }
        }
        tasks.named<SpotBugsTask>("spotbugsTest") {
            enabled = false
        }

        tasks.named<JavaCompile>("compileJava") {
            dependsOn("processResources")
            options.release.set(19)
            options.compilerArgs.addAll(arrayOf("--enable-preview"))
        }
        tasks.named<JavaCompile>("compileTestJava") {
            options.release.set(19)
            options.compilerArgs.addAll(arrayOf("--enable-preview"))
        }

        tasks.named<Test>("test") {
            useJUnitPlatform()
            // report is always generated after tests run
            finalizedBy(tasks.named("jacocoTestReport"))
        }

        tasks.named<JacocoReport>("jacocoTestReport") {
            // tests are required to run before generating the report
            dependsOn(tasks.named("test"))
            reports {
                csv.required.set(true)
                xml.required.set(true)
            }
        }

        configure<PublishingExtension> {
            repositories {
                maven {
                    url = uri("https://gitlab.com/api/v4/projects/43837012/packages/maven")
                    credentials(HttpHeaderCredentials::class) {
                        val gitLabTokenName: String? by project
                        val gitLabTokenValue: String? by project
                        // Set these in your ~/.gradle/gradle.properties:
                        // gitLabTokenName=Private-Token
                        // gitLabTokenValue=<YOUR PRIVATE TOKEN>

                        name = gitLabTokenName ?: "Job-Token"
                        value = gitLabTokenValue ?: System.getenv("CI_JOB_TOKEN")
                    }
                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }
        }
    }
}

defaultTasks("spotlessApply", "build")
