plugins {
    id("java-library")
}

description = "jsql-pgserver"

val String.v: String get() = rootProject.extra["$this.version"] as String

dependencies {
    implementation(project(":pgwire"))
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "org.pgwire"
            artifactId = "pgserver"
            version = version

            from(components["java"])
        }
    }
}

defaultTasks("spotlessApply", "build")
