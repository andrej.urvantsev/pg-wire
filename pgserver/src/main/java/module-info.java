module org.pgwire.pgserver {
  requires java.base;
  requires java.logging;
  requires org.pgwire.protocol;

  exports org.pgwire.pgserver;
}
