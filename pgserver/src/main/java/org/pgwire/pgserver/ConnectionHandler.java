package org.pgwire.pgserver;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HexFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.pgwire.protocol.messages.ClientSimpleQuery;
import org.pgwire.protocol.messages.ClientTerminate;
import org.pgwire.protocol.messages.ServerAuthenticationRequest;
import org.pgwire.protocol.messages.ServerCommandComplete;
import org.pgwire.protocol.messages.ServerDataRow;
import org.pgwire.protocol.messages.ServerKeyData;
import org.pgwire.protocol.messages.ServerReadyForQuery;
import org.pgwire.protocol.messages.ServerRowDescription;
import org.pgwire.protocol.messages.StartupCancelRequest;
import org.pgwire.protocol.messages.StartupGSSENCRequest;
import org.pgwire.protocol.messages.StartupProtocol30;
import org.pgwire.protocol.messages.StartupSSLRequest;

class ConnectionHandler implements Runnable {
  private static final Logger LOGGER = Logger.getLogger(ConnectionHandler.class.getName());

  private final PGConnection pgConnection;

  ConnectionHandler(PGConnection pgConnection) {
    this.pgConnection = pgConnection;
  }

  @Override
  public final void run() {
    String serverChannelId =
        "0x" + HexFormat.of().toHexDigits(System.identityHashCode(pgConnection));
    Thread.currentThread().setName("jsql socket " + serverChannelId);

    var shouldContinue = true;
    while (shouldContinue) {
      var messageBuffer = pgConnection.read();
      var message = pgConnection.getSession().decode(messageBuffer);
      shouldContinue =
          switch (message) {
            case StartupSSLRequest ssl -> handleSSLRequest(ssl);
            case StartupProtocol30 startup -> handleStartupMessage(startup);
            case StartupCancelRequest
            startupCancelRequest -> throw new UnsupportedOperationException();
            case StartupGSSENCRequest
            startupGSSENCRequest -> throw new UnsupportedOperationException();
            case ClientSimpleQuery query -> handleQueryMessage(query);
            case ClientTerminate ignored -> false;
          };
    }
    pgConnection.close();
    LOGGER.fine("socket closed");
  }

  // Where each of those methods contains the previous logic it held in the "if/else" statement, for
  // example:
  private boolean handleSSLRequest(StartupSSLRequest sslRequest) {
    System.out.println("[SERVER] SSL Request: " + sslRequest);
    ByteBuffer sslResponse = ByteBuffer.allocate(1);
    sslResponse.put((byte) 'N');
    sslResponse.flip();
    try {
      pgConnection.write(sslResponse);
    } catch (Exception ex) {
      LOGGER.log(Level.SEVERE, "Failed to write SSL response", ex);
      return false;
    }
    return true;
  }

  private boolean handleStartupMessage(StartupProtocol30 startup) {
    System.out.println("[SERVER] Startup Message: " + startup);

    // Then, write AuthenticationOk
    ServerAuthenticationRequest authRequest = new ServerAuthenticationRequest();
    pgConnection.write(authRequest.encode());

    // Then, write ServerKeyData
    ServerKeyData backendKeyData = new ServerKeyData(1234, 5678);
    pgConnection.write(backendKeyData.encode());

    // Then, write ServerReadyForQuery
    ServerReadyForQuery readyForQuery = new ServerReadyForQuery();
    pgConnection.write(readyForQuery.encode());

    return true;
  }

  private boolean handleQueryMessage(ClientSimpleQuery query) {
    System.out.println("[SERVER] Query Message: " + query);

    // Let's assume it's a query message, and just send a simple response
    // First we send a ServerRowDescription. We'll send two columns, with names "id" and "name"
    ServerRowDescription rowDescription =
        new ServerRowDescription(
            List.of(
                new ServerRowDescription.Field("id", 0, 0, 23, 4, -1, 0),
                new ServerRowDescription.Field("name", 0, 0, 25, -1, -1, 0)));
    pgConnection.write(rowDescription.encode());

    // Then we send a ServerDataRow for each row. We'll send two rows, with values (1, "one") and
    // (2,
    // "two")
    ServerDataRow dataRow1 =
        new ServerDataRow(
            List.of(
                ByteBuffer.wrap("1".getBytes(StandardCharsets.UTF_8)),
                ByteBuffer.wrap("one".getBytes(StandardCharsets.UTF_8))));
    pgConnection.write(dataRow1.encode());

    ServerDataRow dataRow2 =
        new ServerDataRow(
            List.of(
                ByteBuffer.wrap("2".getBytes(StandardCharsets.UTF_8)),
                ByteBuffer.wrap("two".getBytes(StandardCharsets.UTF_8))));

    pgConnection.write(dataRow2.encode());

    // We send a ServerCommandComplete
    ServerCommandComplete commandComplete = new ServerCommandComplete("SELECT 2");
    pgConnection.write(commandComplete.encode());

    // Finally, write ServerReadyForQuery
    ServerReadyForQuery readyForQuery = new ServerReadyForQuery();
    pgConnection.write(readyForQuery.encode());

    return true;
  }
}
