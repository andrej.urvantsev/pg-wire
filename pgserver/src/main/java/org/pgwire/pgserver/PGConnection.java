package org.pgwire.pgserver;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import org.pgwire.protocol.PGSession;

public class PGConnection implements AutoCloseable {
  private static final int BUFFER_LENGTH = 8 * 1024;

  private final ByteBuffer readBuffer = ByteBuffer.allocate(BUFFER_LENGTH);

  private final PGSession session;

  private final Socket delegate;
  private final ReadableByteChannel inputChannel;
  private final WritableByteChannel outputChannel;

  /**
   * Plain socket.
   *
   * @param delegate delegate socket
   */
  protected PGConnection(Socket delegate) {
    session = new PGSession();
    this.delegate = delegate;
    try {
      this.inputChannel = Channels.newChannel(delegate.getInputStream());
      this.outputChannel = Channels.newChannel(delegate.getOutputStream());
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  /**
   * Create a new server socket.
   *
   * @param delegate underlying socket
   * @return a new plain socket
   */
  public static PGConnection server(Socket delegate) {
    return new PGConnection(delegate);
  }

  public PGSession getSession() {
    return session;
  }

  @Override
  public void close() {
    try {
      delegate.close();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  public void write(ByteBuffer buffer) {
    try {
      outputChannel.write(buffer);
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  public ByteBuffer read() {
    readBuffer.clear();
    try {
      inputChannel.read(readBuffer);
      readBuffer.flip();
      return readBuffer;
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
