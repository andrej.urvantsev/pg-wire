package org.pgwire.pgserver;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.HexFormat;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerListener {
  private static final int BACKLOG = 100;
  private static final String socketName = "pg-wire";
  private static final long EXECUTOR_SHUTDOWN_MILLIS = 500L;

  private static final Logger LOGGER = Logger.getLogger(ServerListener.class.getName());

  private final ExecutorService readerExecutor;
  private final Thread serverThread;
  private final InetSocketAddress configuredAddress;
  private final CompletableFuture<Void> closeFuture;

  private volatile boolean running;
  private volatile int connectedPort;
  private volatile ServerSocket serverSocket;

  public static void main(String... args) throws Exception {
    var server = new ServerListener(InetAddress.getByName("0.0.0.0"), 5432);
    server.start();
  }

  public ServerListener(InetAddress listenerAddress, int listenerPort) {
    serverThread =
        Thread.ofPlatform()
            .daemon(false)
            .name("server-" + socketName + "-listener")
            .unstarted(this::listen);

    // to read requests and execute tasks
    this.readerExecutor = Executors.newThreadPerTaskExecutor(Thread.ofVirtual().factory());

    closeFuture = new CompletableFuture<>();

    int port = listenerPort;
    if (port < 1) {
      port = 0;
    }
    configuredAddress = new InetSocketAddress(listenerAddress, port);
  }

  void start() {
    try {
      serverSocket = new ServerSocket();
      serverSocket.bind(configuredAddress, BACKLOG);
    } catch (IOException ex) {
      throw new UncheckedIOException("Failed to start server", ex);
    }

    String serverChannelId =
        "0x" + HexFormat.of().toHexDigits(System.identityHashCode(serverSocket));

    running = true;

    InetAddress inetAddress = serverSocket.getInetAddress();
    connectedPort = serverSocket.getLocalPort();

    if (LOGGER.isLoggable(Level.INFO)) {
      LOGGER.info(
          "[%s] http://%s:%d bound for socket '%s'"
              .formatted(serverChannelId, inetAddress.getHostAddress(), connectedPort, socketName));
    }

    serverThread.start();
  }

  void stop() {
    if (!running) {
      return;
    }
    running = false;
    try {
      // Stop listening for connections
      serverSocket.close();

      // Shutdown reader executor
      try {
        readerExecutor.awaitTermination(EXECUTOR_SHUTDOWN_MILLIS, TimeUnit.MILLISECONDS);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        LOGGER.log(Level.WARNING, "Exception thrown on reader executor termination", e);
      }
      if (!readerExecutor.isTerminated()) {
        LOGGER.log(Level.WARNING, "Some tasks in reader executor did not terminate gracefully");
        readerExecutor.shutdown();
      }
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Exception thrown on socket close", e);
    }
    serverThread.interrupt();
    closeFuture.join();
  }

  private void listen() {
    String serverChannelId =
        "0x" + HexFormat.of().toHexDigits(System.identityHashCode(serverSocket));

    while (running) {
      try {
        // if accept fails itself, we consider it end of story, the listener is broken
        var socket = serverSocket.accept();

        try {
          var pgConnection = PGConnection.server(socket);
          var handler = new ConnectionHandler(pgConnection);
          readerExecutor.execute(handler);
        } catch (RejectedExecutionException e) {
          LOGGER.log(Level.SEVERE, "Executor rejected handler for new connection");
        } catch (Exception e) {
          LOGGER.log(Level.SEVERE, "Failed to handle accepted socket", e);
        }
      } catch (SocketException ex) {
        if (!ex.getMessage().contains("Socket closed")) {
          LOGGER.log(Level.SEVERE, "Failed to handle socket", ex);
        }
        if (running) {
          stop();
        }
      } catch (Exception ex) {
        LOGGER.log(Level.SEVERE, "Unexpected exception", ex);
        if (running) {
          stop();
        }
      }
    }

    if (LOGGER.isLoggable(Level.INFO)) {
      LOGGER.info("[%s] %s socket closed.".formatted(serverChannelId, socketName));
    }
    closeFuture.complete(null);
  }
}
