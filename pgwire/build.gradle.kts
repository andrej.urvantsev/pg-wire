plugins {
    id("java-library")
    id("me.champeau.jmh")
}

description = "jsql-pgwire"

val String.v: String get() = rootProject.extra["$this.version"] as String

dependencies {
}

jmh {
    jmhVersion.set("1.36")
    warmupIterations.set(2)
    iterations.set(2)
    fork.set(2)

    humanOutputFile.set(project.file("${project.buildDir}/reports/jmh/human.txt"))
}

tasks {
    checkstyleJmh {
        enabled = false
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            groupId = "org.pgwire"
            artifactId = "pgwire"
            version = version

            from(components["java"])
            pom {
                name.set("PgWire")
                description.set("Java implementation of PostgreSQL frontend/backend protocol")
                url.set("https://gitlab.com/andrej.urvantsev/pg-wire")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://gitlab.com/andrej.urvantsev/pg-wire/-/blob/main/LICENSE")
                    }
                }
            }

        }
    }
}

defaultTasks("spotlessApply", "build")
