module org.pgwire.protocol {
  requires java.base;
  requires java.logging;

  exports org.pgwire.protocol;
  exports org.pgwire.protocol.messages;
}
