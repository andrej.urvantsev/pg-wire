package org.pgwire.protocol;

import java.nio.ByteBuffer;
import org.pgwire.protocol.messages.ClientSimpleQuery;
import org.pgwire.protocol.messages.ClientTerminate;
import org.pgwire.protocol.messages.PGClientMessage;
import org.pgwire.protocol.messages.PGStartupMessage;
import org.pgwire.protocol.messages.StartupProtocol30;
import org.pgwire.protocol.messages.StartupSSLRequest;
import org.pgwire.protocol.messages.UnknownMessageException;

public final class PGSession {

  private PGSessionState sessionState;

  public PGSession() {
    this.sessionState = PGSessionState.CONNECTED;
  }

  public PGClientMessage decode(ByteBuffer buffer) {
    if (!buffer.hasRemaining()) {
      throw new IllegalArgumentException("Received empty buffer");
    }

    // Startup message
    if (sessionState == PGSessionState.CONNECTED) {
      // We expect either SSL request or startup packet nothing else
      var msg = PGStartupMessage.decode(buffer);
      if (msg instanceof StartupSSLRequest) {
        sessionState = PGSessionState.SSL_REQUEST;
      } else if (msg instanceof StartupProtocol30) {
        sessionState = PGSessionState.STARTUP;
      }
      return msg;
    } else if (sessionState == PGSessionState.SSL_REQUEST) {
      // Only startup packet is expected
      var msg = PGStartupMessage.decode(buffer);
      if (msg instanceof StartupProtocol30) {
        sessionState = PGSessionState.STARTUP;
        return msg;
      }
      throw new UnknownMessageException();
    }

    // All other messages have a char tag at the beginning
    return switch ((char) buffer.get(0)) {
      case 'Q' -> ClientSimpleQuery.decode(buffer);
      case 'X' -> ClientTerminate.decode(buffer);
      default -> throw new UnknownMessageException();
    };
  }

  private enum PGSessionState {
    CONNECTED,
    SSL_REQUEST,
    STARTUP
  }
}
