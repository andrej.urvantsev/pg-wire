package org.pgwire.protocol.messages;

import java.lang.foreign.MemorySegment;
import java.nio.ByteBuffer;

public record ClientSimpleQuery(String query) implements PGClientMessage {

  public static ClientSimpleQuery decode(ByteBuffer buffer) {
    // TODO: Length validation
    var query = MemorySegment.ofBuffer(buffer).getUtf8String(5);
    return new ClientSimpleQuery(query);
  }
}
