package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * The normal, graceful termination procedure is that the frontend sends a Terminate message and
 * immediately closes the connection. On receipt of this message, the backend closes the connection
 * and terminates.
 */
public record ClientTerminate() implements PGClientMessage {

  private static final byte[] MSG_MASK = new byte[] {'X', 0, 0, 0, 4};

  public static ClientTerminate decode(ByteBuffer buffer) {
    if (buffer.remaining() < 5) {
      throw new UnknownMessageException();
    }

    var bufferMsg = new byte[5];
    buffer.get(bufferMsg);
    if (Arrays.equals(MSG_MASK, bufferMsg)) {
      return new ClientTerminate();
    } else {
      throw new UnknownMessageException();
    }
  }
}
