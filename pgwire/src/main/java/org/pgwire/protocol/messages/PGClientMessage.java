package org.pgwire.protocol.messages;

public sealed interface PGClientMessage
    permits ClientSimpleQuery, ClientTerminate, PGStartupMessage {}
