package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;

public sealed interface PGServerMessage
    permits ServerAuthenticationRequest,
        ServerKeyData,
        ServerCommandComplete,
        ServerDataRow,
        ServerReadyForQuery,
        ServerRowDescription {

  ByteBuffer encode();
}
