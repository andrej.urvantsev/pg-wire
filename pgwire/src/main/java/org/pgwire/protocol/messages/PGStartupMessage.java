package org.pgwire.protocol.messages;

import java.lang.foreign.MemorySegment;
import java.nio.ByteBuffer;
import java.util.HashMap;

public sealed interface PGStartupMessage extends PGClientMessage
    permits StartupCancelRequest, StartupGSSENCRequest, StartupProtocol30, StartupSSLRequest {

  static PGStartupMessage decode(ByteBuffer buffer) {
    if (buffer.remaining() < 8) {
      throw new UnknownMessageException();
    }
    var length = buffer.getInt();
    if (buffer.remaining() < (length - 4)) {
      throw new UnknownMessageException();
    }

    var protocol = buffer.getInt();
    if (protocol == StartupSSLRequest.SSL_REQUEST) {
      return new StartupSSLRequest();
    } else if (protocol == StartupGSSENCRequest.GSSENC_REQUEST) {
      return new StartupGSSENCRequest();
    } else if (protocol == StartupCancelRequest.CANCEL_REQUEST) {
      return new StartupCancelRequest();
    }

    if (protocol != StartupProtocol30.PROTOCOL_3_0) {
      throw new UnknownMessageException();
    }

    // Startup message data
    var segment = MemorySegment.ofBuffer(buffer);
    var parameters = new HashMap<String, String>();
    var offset = 0;
    while (offset < length - 9) {
      var name = segment.getUtf8String(offset);
      offset += name.length() + 1;
      var value = segment.getUtf8String(offset);
      offset += value.length() + 1;
      parameters.put(name, value);
    }
    return new StartupProtocol30(protocol, parameters);
  }
}
