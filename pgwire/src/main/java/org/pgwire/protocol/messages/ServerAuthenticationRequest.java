package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;

public record ServerAuthenticationRequest() implements PGServerMessage {
  @Override
  public ByteBuffer encode() {
    var buffer = ByteBuffer.allocate(9);
    buffer.put((byte) 'R'); // 'R' for ServerAuthenticationRequest
    buffer.putInt(8); // Length
    buffer.putInt(0); // Authentication type, 0 for OK
    buffer.flip();
    return buffer;
  }
}
