package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public record ServerCommandComplete(String commandTag) implements PGServerMessage {
  @Override
  public ByteBuffer encode() {
    var commandTag = commandTag();
    var length = 4 + commandTag.length() + 1;
    return ByteBuffer.allocate(length + 1) // +1 for msg type
        .put((byte) 'C')
        .putInt(length) // +4 for length
        .put(commandTag.getBytes(StandardCharsets.UTF_8))
        .put((byte) 0) // null terminator
        .flip();
  }
}
