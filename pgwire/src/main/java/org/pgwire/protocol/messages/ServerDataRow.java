package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;
import java.util.List;

public record ServerDataRow(List<ByteBuffer> values) implements PGServerMessage {
  @Override
  public ByteBuffer encode() {
    // For each value, we need to add 4 bytes for the length, plus the length of the value
    var length = 4 + 2 + values().stream().map(it -> it.remaining() + 4).reduce(0, Integer::sum);
    var buffer =
        ByteBuffer.allocate(length + 1) // +1 for msg type
            .put((byte) 'D')
            .putInt(length) // +4 for length
            .putShort((short) values().size()); // +2 for number of columns
    for (var value : values()) {
      buffer.putInt(value.remaining());
      buffer.put(value);
    }
    buffer.flip();
    return buffer;
  }
}
