package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;

public record ServerKeyData(int processId, int secretKey) implements PGServerMessage {
  @Override
  public ByteBuffer encode() {
    var buffer = ByteBuffer.allocate(13);
    buffer.put((byte) 'K'); // 'K' for ServerKeyData
    buffer.putInt(12); // Length
    buffer.putInt(processId()); // Process ID
    buffer.putInt(secretKey()); // Secret key
    buffer.flip();
    return buffer;
  }
}
