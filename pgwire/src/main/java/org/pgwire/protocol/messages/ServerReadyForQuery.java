package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;

public record ServerReadyForQuery() implements PGServerMessage {
  @Override
  public ByteBuffer encode() {
    var buffer = ByteBuffer.allocate(6);
    buffer.put((byte) 'Z'); // 'Z' for ServerReadyForQuery
    buffer.putInt(5); // Length
    buffer.put((byte) 'I'); // Transaction status indicator, 'I' for idle
    buffer.flip();
    return buffer;
  }
}
