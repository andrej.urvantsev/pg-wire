package org.pgwire.protocol.messages;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

public record ServerRowDescription(List<Field> fields) implements PGServerMessage {
  @Override
  public ByteBuffer encode() {
    var length = 4 + 2 + fields().stream().mapToInt(ServerRowDescription.Field::length).sum();
    var buffer =
        ByteBuffer.allocate(length + 1)
            .put((byte) 'T')
            .putInt(length)
            .putShort((short) fields.size());
    fields()
        .forEach(
            field ->
                buffer
                    .put(field.name().getBytes(StandardCharsets.UTF_8))
                    .put((byte) 0) // null-terminated
                    .putInt(field.tableObjectId())
                    .putShort((short) field.columnAttributeNumber())
                    .putInt(field.dataTypeObjectId())
                    .putShort((short) field.dataTypeSize())
                    .putInt(field.typeModifier())
                    .putShort((short) field.formatCode()));
    buffer.flip();
    return buffer;
  }

  public record Field(
      String name,
      int tableObjectId,
      int columnAttributeNumber,
      int dataTypeObjectId,
      int dataTypeSize,
      int typeModifier,
      int formatCode) {

    int length() {
      // 4 (int tableObjectId) + 2 (short columnAttributeNumber) + 4 (int dataTypeObjectId) + 2
      // (short
      // dataTypeSize) + 4 (int typeModifier) + 2 (short formatCode)
      // 4 + 2 + 4 + 2 + 4 + 2 = 18
      // Add name length, plus 1 for null terminator '\0'
      return 18 + name.length() + 1;
    }
  }
}
