package org.pgwire.protocol.messages;

public record StartupGSSENCRequest() implements PGStartupMessage {

  /**
   * The GSSAPI Encryption request code. The value is chosen to contain 1234 in the most significant
   * 16 bits, and 5680 in the least significant 16 bits. (To avoid confusion, this code must not be
   * the same as any protocol version number.)
   */
  public static final int GSSENC_REQUEST = 0x4D21630;
}
