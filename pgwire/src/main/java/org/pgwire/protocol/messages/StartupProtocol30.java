package org.pgwire.protocol.messages;

import java.util.Map;

public record StartupProtocol30(int protocol, Map<String, String> parameters)
    implements PGStartupMessage {
  /**
   * The protocol version number. The most significant 16 bits are the major version number (3 for
   * the protocol described here). The least significant 16 bits are the minor version number (0 for
   * the protocol described here).
   */
  public static final int PROTOCOL_3_0 = 0x30000;
}
