package org.pgwire.protocol.messages;

public record StartupSSLRequest() implements PGStartupMessage {

  /**
   * The SSL request code. The value is chosen to contain 1234 in the most significant 16 bits, and
   * 5679 in the least significant 16 bits. (To avoid confusion, this code must not be the same as
   * any protocol version number.)
   */
  public static final int SSL_REQUEST = 0x4D2162F;
}
