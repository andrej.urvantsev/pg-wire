rootProject.name = "pg-wire"

pluginManagement {
    plugins {
        // https://github.com/spotbugs/spotbugs-gradle-plugin
        id("com.github.spotbugs") version "5.0.13"

        // https://github.com/diffplug/spotless/tree/main/plugin-gradle
        id("com.diffplug.spotless") version "6.15.0"

        // https://github.com/ben-manes/gradle-versions-plugin
        id("com.github.ben-manes.versions") version "0.46.0"

        // https://github.com/melix/jmh-gradle-plugin
        id("me.champeau.jmh") version "0.7.0"

        // https://github.com/researchgate/gradle-release
        id("net.researchgate.release") version "3.0.2"
    }
}

include("pgwire")
include("pgserver")
